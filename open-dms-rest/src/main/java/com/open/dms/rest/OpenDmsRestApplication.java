package com.open.dms.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenDmsRestApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenDmsRestApplication.class, args);
    }

}
