package com.open.dms.persistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenDmsPersistenceApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenDmsPersistenceApplication.class, args);
    }

}
