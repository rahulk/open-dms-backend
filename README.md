## Open DMS Backend
Repository for everything backend

---

## Project structure
The project consists of the multiple sub-modules. Refer respective README for in-depth details. 

| Module               | Description                                                       |
|----------------------|-------------------------------------------------------------------|
| open-dms-persistence | Module for hosting persistence related logic, e.g. using postgres |
| open-dms-search      | Module for full text searching/filtering capabilities             |
| open-dms-security    | Authentication and authorization logic                            |
| open-dms-rest        | Contains REST endpoints and API documentation                     |

---
## Running Locally

### Checkstyle
1. Checkstyle used : https://checkstyle.sourceforge.io/sun_style.html
2. Use Checkstyle plugin in Intellij for better experience, configure using config/checkstyle/checkstyle.xml
