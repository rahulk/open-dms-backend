package com.open.dms.search;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenDmsSearchApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenDmsSearchApplication.class, args);
    }

}
