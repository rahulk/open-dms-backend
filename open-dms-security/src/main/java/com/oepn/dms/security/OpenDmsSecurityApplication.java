package com.oepn.dms.security;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OpenDmsSecurityApplication {

    public static void main(String[] args) {
        SpringApplication.run(OpenDmsSecurityApplication.class, args);
    }

}
